import argparse
import os
import time

import gym
import numpy as np
import tensorflow as tf


import src.layers as layers
from src.network import Network
from src.ga_train import train


class GANetwork(Network):
    def __init__(self, input_op, output_size, is_training=False):
        self.input_op = input_op
        self.layers = []
        if is_training is not None:
            layers.Layer.IS_TRAINING = is_training

        network_output = self.fully_connected(input_op, 16)
        network_output = self.fully_connected(network_output, 16)
        network_output = self.fully_connected(network_output, output_size, activation=None, name='Final_FC')
        self.output_op = network_output


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--output-dir', default=os.path.join('output', 'cartpole_ga'),
                        help='Path to output train files')
    arguments = parser.parse_args()

    def create_env():
        return gym.make('CartPole-v1')
    def reward_function(_, done, max_step, step, last_fall):
        if done:
            return -(max_step - (step - last_fall))
        return 1


    os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
    gpu_options = tf.GPUOptions(allow_growth=True)
    session_config = tf.ConfigProto(gpu_options=gpu_options)

    population_size = 50
    max_generation = 10
    fitness_max_step = 1000

    with tf.Session(config=session_config).as_default() as session:
        agent, scores = train(
            session, GANetwork, create_env,
            population_size, max_generation, fitness_max_step, 5, 4, arguments.output_dir,
            mutation_stdev=0.02, reward_function=reward_function)
        best_network = np.argmax(scores)

        # Test
        max_step = 5000
        env = create_env()
        env._max_episode_steps = max_step
        env.render()
        time.sleep(1)
        state = env.reset()
        for iteration in range(max_step):
            env.render()
            action = np.argmax(agent.predict(best_network, [state])[0])
            state, _, done, _ = env.step(action)
            if done:
                break
        env.close()
        print('Best network did {} iterations in test env'.format(iteration))


if __name__ == '__main__':
    main()
