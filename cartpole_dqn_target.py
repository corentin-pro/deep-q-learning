import os
import random
import shutil

from collections import deque

import gym
import numpy as np
import tensorflow as tf


import src.layers as layers
from src.network import Network


class DQNetwork(Network):
    def __init__(self, input_op, output_size, is_training=False):
        self.input_op = input_op
        self.layers = []
        if is_training is not None:
            layers.Layer.IS_TRAINING = is_training

        network_output = self.fully_connected(input_op, 32)
        network_output = self.fully_connected(network_output, 32)
        network_output = self.fully_connected(network_output, output_size, activation=None, name='Final_FC')
        self.output_op = network_output


class DQAgent:
    def __init__(self, state_shape, action_shape, memory_size, tf_session):
        self.state_shape = state_shape
        self.action_shape = action_shape
        self.memory = deque(maxlen=memory_size)
        self.discount_rate = 0.95
        self.exploration_rate = 1.0
        self.exploration_min = 0.01
        self.exploration_decay = 0.998
        self.learning_rate = 0.001
        self.target_update_step = 100

        self.tf_session = tf_session
        self.tf_output_dir = 'output'
        self.tf_summaries = []
        self.tf_global_step = 0

        self.input_placeholder = tf.placeholder(tf.float32, shape=[None, self.state_shape])
        self.label_placeholder = tf.placeholder(tf.float32, shape=[None, self.action_shape])
        self.result_placeholder = tf.placeholder(tf.float32, shape=[2])
        with tf.variable_scope('Metrics'):
            self.tf_result_metric = tf.summary.merge(
                [tf.summary.scalar('iteration', self.result_placeholder[0]),
                 tf.summary.scalar('episode', self.result_placeholder[1])])
        with tf.variable_scope('Network'):
            layers.Layer.VERBOSE = 1
            layers.Layer.METRICS = False
            with tf.variable_scope('Action'):
                self.network = DQNetwork(self.input_placeholder, self.action_shape, True)
                self.tf_summaries += [layer.summaries.histograms for layer in self.network.layers]
            with tf.variable_scope('Target'):
                self.target_network = DQNetwork(self.input_placeholder, self.action_shape, True)

        with tf.variable_scope('Train'):
            loss_op = tf.reduce_mean(tf.square(self.network.output_op - self.label_placeholder))
            optimizer = tf.train.AdamOptimizer(self.learning_rate)
            # optimizer = tf.train.MomentumOptimizer(self.learning_rate, 0.5)
            self.train_op = optimizer.minimize(loss_op)
            self.tf_summaries.append(tf.summary.scalar('loss', tf.reduce_mean(loss_op)))

            # Get the parameters of our DQNNetwork
            action_vars = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, "Network/Action")
            # Get the parameters of our Target_network
            target_vars = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, "Network/Target")
            self.target_update_op = []
            # Update our target_network parameters with DQNNetwork parameters
            for action_var, target_var in zip(action_vars, target_vars):
                self.target_update_op.append(target_var.assign(action_var))

        if self.tf_output_dir:
            if os.path.exists(self.tf_output_dir):
                shutil.rmtree(self.tf_output_dir)
            os.makedirs(self.tf_output_dir)

            # Saving the configuration as JSON files
            self.summary_op = tf.summary.merge(self.tf_summaries)

            # Create writers and savers
            self.tf_train_writer = tf.summary.FileWriter(
                os.path.join(self.tf_output_dir, "train"), self.tf_session.graph, flush_secs=3)
            self.tf_saver = tf.train.Saver(tf.global_variables(scope="Network/Action"), max_to_keep=5)
        else:
            self.tf_train_writer = None


    def remember(self, state, action, reward, next_state, done):
        self.memory.append((state, action, reward, next_state, done))

    def predict(self, state):
        return self.tf_session.run(
            self.network.output_op, feed_dict={self.input_placeholder: state})

    def predict_target(self, state):
        return self.tf_session.run(
            self.target_network.output_op, feed_dict={self.input_placeholder: state})

    def act(self, state):
        if np.random.rand() <= self.exploration_rate:
            return random.randrange(self.action_shape)
        return np.argmax(self.predict([state])[0])

    def replay(self, batch_size):
        shuffled_data = random.sample(self.memory, batch_size)
        states = []
        targets = []
        # for state, action, reward, next_state, done in shuffled_data:
        #     target = reward
        #     if not done:
        #         target = reward + self.discount_rate * np.amax(self.predict_target(next_state)[0])
        #     target_output = self.predict(state)[0]
        #     target_output[action] = target
        #     states.append(state[0])
        #     targets.append(target_output)
        predicted_taret = self.predict([data[0] for data in shuffled_data])
        max_next_target = self.discount_rate * np.amax(self.predict_target([data[3] for data in shuffled_data]), axis=1)
        for data_index, data in enumerate(shuffled_data):
            state, action, reward, _, done = data
            if not done:
                target = reward + max_next_target[data_index]
            else:
                target = reward
            target_output = predicted_taret[data_index]
            target_output[action] = target
            states.append(state)
            targets.append(target_output)
        # Training
        self.tf_session.run(self.train_op, feed_dict={
            self.input_placeholder: states,
            self.label_placeholder: targets})
        # Saving metrics
        if self.tf_global_step % 100 == 0:
            summary = self.tf_session.run(self.summary_op, feed_dict={
                self.input_placeholder: states,
                self.label_placeholder: targets})
            self.tf_train_writer.add_summary(summary, global_step=self.tf_global_step)
        # Updating target network
        if self.tf_global_step % self.target_update_step == 0:
            self.tf_session.run(self.target_update_op)
        self.tf_global_step += 1
        # Reducing ecploration rate
        if self.exploration_rate > self.exploration_min:
            self.exploration_rate *= self.exploration_decay

    def save(self, name):
        self.tf_saver.save(self.tf_session, os.path.join(self.tf_output_dir, name))

    def result_metric(self, result):
        summary = self.tf_session.run(self.tf_result_metric, feed_dict={self.result_placeholder: result})
        self.tf_train_writer.add_summary(summary, global_step=self.tf_global_step)

def main():
    env = gym.make('CartPole-v1')
    env.render()

    os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
    gpu_options = tf.GPUOptions(allow_growth=True)
    session_config = tf.ConfigProto(gpu_options=gpu_options)

    max_step = 500
    max_episode = 500
    batch_size = 32

    with tf.Session(config=session_config).as_default() as session:
        state_size = env.observation_space.shape[0]
        action_size = env.action_space.n

        agent = DQAgent(state_size, action_size, 200, session)
        session.run(tf.global_variables_initializer())

        agent.save('checkpoint_0')
        for episode in range(max_episode):
            # print('Episode {}'.format(episode))
            state = env.reset()
            for step in range(max_step):
                env.render()
                action = agent.act(state)
                next_state, reward, done, _ = env.step(action)
                reward = 1 if not done else -1
                agent.remember(state, action, reward, next_state, done)
                state = next_state
                if done:
                    agent.result_metric([step, episode])
                    print('Episode {} finished after {} steps (exploration rate : {:.03f})   '.format(
                        episode, step + 1, agent.exploration_rate), end='\r')
                    break
                if len(agent.memory) > batch_size:
                    agent.replay(batch_size)
            if not done:
                agent.result_metric([step, episode])
                print('Episode {} : Success!             '.format(episode))
        agent.save('checkpoint_{}'.format(episode))
        print()
        env.close()


if __name__ == '__main__':
    main()
