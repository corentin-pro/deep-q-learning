import tensorflow as tf

import src.layers as layers


class Network():

    def __init__(self, input_op):
        self.input_op = input_op
        self.layers = []
        self.output_op = None

    def conv1D(self, input_op, output_features, **kwargs):
        convolution = layers.Conv1D(input_op, output_features, **kwargs)
        self.layers.append(convolution)
        return convolution.output_op

    def deconv1D(self, input_op, **kwargs):
        deconvolution = layers.DeConv1D(input_op, **kwargs)
        self.layers.append(deconvolution)
        return deconvolution.output_op

    def conv2D(self, input_op, output_features, **kwargs):
        convolution = layers.Conv2D(input_op, output_features, **kwargs)
        self.layers.append(convolution)
        return convolution.output_op

    def deconv2D(self, input_op, **kwargs):
        deconvolution = layers.DeConv2D(input_op, **kwargs)
        self.layers.append(deconvolution)
        return deconvolution.output_op

    def skip_connection(self, input_op, skip_op, output_features, name="SkipCon", **kwargs):
        with tf.variable_scope(name):
            output_op = tf.concat([input_op, skip_op], axis=-1)
            output_op = layers.Conv2D(output_op, output_features, name="1x1", **kwargs)

    def fully_connected(self, input_op, output_dimmension, **kwargs):
        fc = layers.FullyConnected(input_op, output_dimmension, **kwargs)
        self.layers.append(fc)
        return fc.output_op

    # def lstm(self, output_dimmension, **kwargs):
    #     lstm = layers.LSTM(output_dimmension, **kwargs)
    #     self.layers.append(lstm)
    #     self.lstm_layers.append(lstm)
    #     return lstm
