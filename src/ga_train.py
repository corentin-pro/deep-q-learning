import os
import shutil
import time

import numpy as np
import tensorflow as tf

import src.layers as layers


class GAAgent:
    def __init__(self, environment_creator, max_step, tf_session, population_size, network_class,
                 mutation_stdev=0.002, mutation_rate=0.1, output_dir='output'):
        self.environment_creator = environment_creator
        env = environment_creator()
        self.state_size = env.observation_space.shape[0]
        self.action_size = env.action_space.n
        env.close()

        self.population_size = population_size
        self.population = None
        self.max_step = max_step
        self.mutation_stdev = mutation_stdev
        self.mutation_rate = mutation_rate

        self.tf_session = tf_session
        self.tf_output_dir = output_dir

        self.input_placeholders = []
        self.result_placeholder = tf.placeholder(tf.float32, shape=[2])
        with tf.variable_scope('Metrics'):
            self.tf_result_metric = tf.summary.merge(
                [tf.summary.scalar('iteration', self.result_placeholder[0]),
                 tf.summary.scalar('episode', self.result_placeholder[1])])

        self.tf_networks = []
        self.tf_network_outputs = []
        self.tf_savers = []
        self.envs = []
        self.tf_network_save = []
        self.tf_network_save_mutated = []
        self.tf_network_load = []
        self.tf_network_init = []
        with tf.variable_scope('Network_fake'):
            layers.Layer.VERBOSE = 1
            layers.Layer.METRICS = False
            input_placeholder = tf.placeholder(tf.float32, shape=[None, self.state_size])
            self.network_fake = network_class(input_placeholder, self.action_size, True)
        fake_vars = tf.get_collection(
            tf.GraphKeys.TRAINABLE_VARIABLES, 'Network_fake')

        for network_index in range(population_size):
            env = self.environment_creator()
            env._max_episode_steps = max_step
            self.envs.append(env)
            input_placeholder = tf.placeholder(tf.float32, shape=[None, self.state_size])
            self.input_placeholders.append(input_placeholder)
            scope_name = 'Network_{}'.format(network_index)
            with tf.variable_scope(scope_name):
                layers.Layer.VERBOSE = 0
                layers.Layer.METRICS = False
                network = network_class(input_placeholder, self.action_size, True)
                self.tf_networks.append(network)
                self.tf_network_outputs.append(network.output_op)
            with tf.variable_scope('Save'):
                global_vars = tf.global_variables(scope=scope_name)
                var_list = {}
                for tf_var in global_vars:
                    var_name = '/'.join(tf_var.name.split('/')[1:])
                    var_list[var_name] = tf_var
                self.tf_savers.append(tf.train.Saver(var_list, max_to_keep=10))

                # In graph assignement from/to fake network
                source_vars = tf.get_collection(
                    tf.GraphKeys.TRAINABLE_VARIABLES, 'Network_{}'.format(network_index))
                save_fake = []
                save_mutated = []
                for source_var, fake_var in zip(source_vars, fake_vars):
                    save_fake.append(fake_var.assign(source_var))
                    # save_mutated.append(fake_var.assign(
                    #     source_var + tf.random_normal(source_var.shape, stddev=0.002)))
                    save_mutated.append(
                        fake_var.assign(
                            source_var + (
                                tf.random_normal(source_var.shape, stddev=self.mutation_stdev) * tf.cast(
                                    tf.less(
                                        tf.random_uniform(source_var.shape),
                                        self.mutation_rate)
                                    , tf.float32)
                            )
                        ))
                self.tf_network_save.append(save_fake)
                self.tf_network_save_mutated.append(save_mutated)
                load_fake = []
                for source_var, fake_var in zip(source_vars, fake_vars):
                    load_fake.append(source_var.assign(fake_var))
                self.tf_network_load.append(load_fake)

                # In graph variable initialize
                self.tf_network_init.append(source_vars)

        if self.tf_output_dir:
            if os.path.exists(self.tf_output_dir):
                shutil.rmtree(self.tf_output_dir)
            os.makedirs(self.tf_output_dir)

            # Create writers and savers
            self.tf_train_writer = tf.summary.FileWriter(
                os.path.join(self.tf_output_dir, "train"), self.tf_session.graph, flush_secs=3)
        else:
            self.tf_train_writer = None

    def initialize(self):
        self.tf_session.run(tf.global_variables_initializer())

    def close(self):
        for env in self.envs:
            env.close()

    def predict(self, network_index, state):
        return self.tf_session.run(
            self.tf_network_outputs[network_index],
            feed_dict={self.input_placeholders[network_index]: state})

    def save_network(self, network_index, name, **kargs):
        self.tf_savers[network_index].save(self.tf_session, os.path.join(self.tf_output_dir, name), **kargs)

    def restore_network(self, network_index, name):
        self.tf_savers[network_index].restore(self.tf_session, os.path.join(self.tf_output_dir, name))

    def copy_network(self, source_index, destination_index):
        self.tf_session.run(self.tf_network_save[source_index])
        self.tf_session.run(self.tf_network_load[destination_index])

    def mutate_network(self, source_index, destination_index):
        self.tf_session.run(self.tf_network_save_mutated[source_index])
        self.tf_session.run(self.tf_network_load[destination_index])

    def init_networks(self, index_list):
        init_op = []
        for index in index_list:
            init_op += self.tf_network_init[index]
        self.tf_session.run(init_op)

    def calculate_fitness(self, reward_function=None, render=None):
        states = [env.reset() for env in self.envs]
        rewards = [0] * self.population_size
        last_fall = [0] * self.population_size
        for step in range(self.max_step):
            if render is not None:
                self.envs[render].render()
            feed_dict = {}
            for network_index in range(self.population_size):
                feed_dict[self.input_placeholders[network_index]] = [states[network_index]]
            actions = np.argmax(self.tf_session.run(self.tf_network_outputs, feed_dict=feed_dict), axis=-1)[:, 0]
            for network_index in range(self.population_size):
                next_state, reward, done, _ = self.envs[network_index].step(actions[network_index])
                if reward_function is not None:
                    rewards[network_index] += reward_function(
                        reward, done, self.max_step, step, last_fall[network_index])
                else:
                    rewards[network_index] += reward
                if done:
                    states[network_index] = self.envs[network_index].reset()
                    last_fall[network_index] = step
                else:
                    states[network_index] = next_state
        if render is not None:
            self.envs[render].close()
        return rewards

    def result_metric(self, result, iteration):
        summary = self.tf_session.run(self.tf_result_metric, feed_dict={self.result_placeholder: result})
        self.tf_train_writer.add_summary(summary, global_step=iteration)


def train(tf_session, network_class, create_env, population_size, max_generation, fitness_max_step,
          selection_best_ratio, selection_ratio, output_dir, reward_function=None, render_period=None,
          mutation_stdev=0.002, mutation_rate=0.1, incremental_fitness=False):
    best_selection = population_size // selection_best_ratio
    worst_selection = selection_ratio * best_selection
    if best_selection + worst_selection > population_size:
        print('Best ratio of {} will use top {} networks and ratio of {} will select {}\
 worst networks but this is larger than the population of {}'.format(
            selection_best_ratio, best_selection, selection_ratio, worst_selection, population_size))
        exit(1)
    print('Best ratio of {} will use top {} networks and ratio of {} will select {} worst networks'.format(
        selection_best_ratio, best_selection, selection_ratio, worst_selection))

    generation_start = time.time()
    agent = GAAgent(
        create_env, fitness_max_step, tf_session, population_size, network_class,
        mutation_stdev=mutation_stdev, mutation_rate=mutation_rate, output_dir=output_dir)
    agent.initialize()
    agent.save_network(0, 'checkpoint-0')
    if incremental_fitness:
        agent.max_step = fitness_max_step // 4

    print('Initialization for a population of {} took {:.03f}s'.format(
        population_size, time.time() - generation_start))

    generation_start = time.time()
    best_index = None
    for generation in range(max_generation):
        scores = np.asarray(agent.calculate_fitness(reward_function=reward_function, render=best_index))
        # print(scores)
        score_args = scores.argsort()
        # excluding worst 10% for output
        sorted_scores = np.sort(scores)[worst_selection:]
        print('Generation {} scores ({} steps) : min {:.03f} , max {:.03f} , mean {:.03f} , testing time {:.02f}s'.format(
            generation, agent.max_step, sorted_scores[0], sorted_scores[-1],
            np.mean(sorted_scores), time.time() - generation_start))
        if incremental_fitness and agent.max_step < fitness_max_step:
            agent.max_step += int(3 * (fitness_max_step / (4 * max_generation)))
        generation_start = time.time()
        if render_period is not None:
            best_index = score_args[-1] if generation % render_period == (render_period - 1) else None
        else:
            best_index = None
        best_networks = score_args[-best_selection:][::-1]
        worst_networks = score_args[:worst_selection]
        mutated_networks = []
        for index, worst_network in enumerate(worst_networks):
            best_network_index = index // selection_ratio
            best_network = best_networks[best_network_index]
            mutated_networks.append((best_network, worst_network))
            agent.mutate_network(best_network, worst_network)
        # print('Mutated network : '+ ' , '.join(['{}=>{}'.format(best, worst) for best, worst in mutated_networks]))
        # agent.init_networks(score_args[worst_selection:-best_selection])
        best_network = np.argmax(scores)
        agent.save_network(best_network, 'checkpoint-best')

    return agent, scores
