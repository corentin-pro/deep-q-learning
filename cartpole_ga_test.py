import argparse
import os
import time

import gym
import numpy as np
import tensorflow as tf


import src.layers as layers
from src.network import Network
from cartpole_ga import GANetwork


class GAAgent:
    def __init__(self, state_size, action_size, tf_session, output_dir='output'):
        self.state_size = state_size
        self.action_size = action_size

        self.tf_session = tf_session
        self.tf_output_dir = output_dir

        self.input_placeholder = tf.placeholder(tf.float32, shape=[None, self.state_size])
        with tf.variable_scope('Network'):
            layers.Layer.VERBOSE = 1
            layers.Layer.METRICS = False
            self.tf_network = GANetwork(self.input_placeholder, self.action_size, False)
            self.tf_network_output = self.tf_network.output_op

            with tf.variable_scope('Save'):
                global_vars = tf.global_variables(scope='Network')
                var_list = {}
                for tf_var in global_vars:
                    var_name = '/'.join(tf_var.name.split('/')[1:])
                    var_list[var_name] = tf_var
                self.tf_saver = tf.train.Saver(var_list, max_to_keep=10)
        self.tf_session.run(tf.global_variables_initializer())


    def restore_network(self, name):
        self.tf_saver.restore(self.tf_session, os.path.join(self.tf_output_dir, name))

    def predict(self, state):
        return self.tf_session.run(
            self.tf_network_output,
            feed_dict={self.input_placeholder: state})


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--output-dir', default=os.path.join('output', 'cartpole_ga'),
                        help='Path to output train files')
    arguments = parser.parse_args()

    max_step = 5000
    env = gym.make('CartPole-v1')
    env._max_episode_steps = max_step
    env.render()

    os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
    gpu_options = tf.GPUOptions(allow_growth=True)
    session_config = tf.ConfigProto(gpu_options=gpu_options)

    state_size = env.observation_space.shape[0]
    action_size = env.action_space.n

    with tf.Session(config=session_config).as_default() as session:
        agent = GAAgent(state_size, action_size, session, output_dir=arguments.output_dir)
        agent.restore_network('checkpoint-best')
        state = env.reset()
        time.sleep(1)
        for iteration in range(max_step):
            env.render()
            action = np.argmax(agent.predict([state])[0])
            state, _, done, _ = env.step(action)
            if done:
                break
        env.close()
        print('Best network did {} iterations in test env'.format(iteration))


if __name__ == '__main__':
    main()
